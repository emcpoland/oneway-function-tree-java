package com.emcpoland.onewayfunctiontree.oft;

import org.junit.Assert;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by mcp on 21/11/15.
 */
public class HashTest {

    @Test
    public void hashTree() {
        Node A = new Node();
        A.leftChild = new Node();
        A.rightChild = new Node();

        Assert.assertArrayEquals(mix(A.leftChild.key,  A.rightChild.key), mix(A.leftChild.key, A.rightChild.key));
    }


    @Test
    public void consistentTree() {
        OneWayFunctionTree tree = new OneWayFunctionTree();
        int noLevels = 8;
        int noNodes = (int)Math.pow(2, noLevels)-1;
        int noLeafs = (noNodes+1)/2;

        Node[] leafs = OFTUtils.addLeafs(tree,noLeafs);

        //OFTUtils.logTree(tree.root);
        Assert.assertTrue(isValidTree(null, tree.root));

        //Assert.assertEquals(tree.root.key, mix(tree.root.leftChild.key));
    }

    private static boolean isValidTree(Node parent, Node node) {
        if(node == null) return true;
        return ((node.leftChild != null && node.rightChild != null && isValidKey(node))
                    || (node.leftChild == null && node.rightChild == null))
                && isValidTree(node, node.leftChild)
                && isValidTree(node, node.rightChild);
    }

    private static boolean isValidKey(Node node) {
        //return node.key.equals(node.rightChild.blindedKey);
        //return node.key.equals(mix(hash(node.leftChild.key), hash(node.rightChild.key)));
        //OFTUtils.logKey(node.key);
        //OFTUtils.logKey(node.rightChild.key);
        //OFTUtils.logKey(node.leftChild.key);
        //return node.key.equals(node.rightChild.key);
        return Arrays.equals(node.key, mix(node.leftChild.blindedKey,  node.rightChild.blindedKey));
    }

    private static byte[] mix(byte[] left, byte[] right) {
        if(left.length!=right.length) throw new RuntimeException("keys not equal length");
        byte[] mixed = new byte[left.length];
        for(int i = 0; i<left.length; i++) {
            mixed[i] = (byte)(left[i]^right[i]);
        }

        //System.out.println("Test");
        //OFTUtils.logKey(left);
        //OFTUtils.logKey(right);
        //OFTUtils.logKey(mixed);
        return mixed;
    }

    private static byte[] hash(byte[] input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(input);
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

}
