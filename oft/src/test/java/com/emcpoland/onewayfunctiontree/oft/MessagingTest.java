package com.emcpoland.onewayfunctiontree.oft;

import com.emcpoland.onewayfunctiontree.MulticastGroup;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import org.junit.Assert;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import sun.java2d.pipe.SpanShapeRenderer;

/**
 * Created by mcp on 23/11/15.
 */
public class MessagingTest {



    @Test
    public void simple() {
        final MulticastGroup group = new MulticastGroup();
        SimpleMGRekeyed simpleMGRekeyed = new SimpleMGRekeyed();
        group.setMGRekeyed(simpleMGRekeyed);
        Map<String,EndPoint> endPoints = simpleMGRekeyed.getEndPoints();

        for(int i=0; i<100; i++) group.add();

        System.out.println();
        OFTUtils.logTree(group.tree.root);
        System.out.println();

        for(EndPoint endPoint:endPoints.values()) {
            System.out.println(endPoint.nodeKey.getId());
            System.out.println("\t"+Base64.encode(endPoint.nodeKey.getKey()));
            if(endPoint.siblingNodeKey!=null) System.out.println("\t"+Base64.encode(endPoint.siblingNodeKey.getKey()));
            for(MulticastGroup.NodeKey nodeKey:endPoint.otherNodeKeys.values()) {
                System.out.println("\t" + Base64.encode(nodeKey.getKey()));
            }
            System.out.println("\t\t"+Base64.encode(endPoint.groupKey));
            System.out.println();
        }

        System.out.println("Groupkey "+Base64.encode(group.tree.root.getKey()));

        for(EndPoint endPoint:endPoints.values()) {
            Assert.assertArrayEquals(group.tree.root.key, endPoint.groupKey);
        }

    }

}
