package com.emcpoland.onewayfunctiontree.oft;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests
 */
public class FindShallowestNodeTest {

    @Test
    public void findShallowestNodeOne() {
        OneWayFunctionTree tree = new OneWayFunctionTree();

        tree.root = new Node();

        Assert.assertEquals(tree.root, tree.findShallowestLeafNode(tree.root, 0).getKey());
    }

    @Test
    public void findShallowestNodeTwo() {
        OneWayFunctionTree tree = new OneWayFunctionTree();

        tree.root = new Node();
        tree.root.leftChild = new Node(tree.root);
        tree.root.rightChild = new Node(tree.root);

        Assert.assertEquals(tree.root.leftChild, tree.findShallowestLeafNode(tree.root, 0).getKey());
    }

    @Test
    public void findShallowestNodeThree() {
        OneWayFunctionTree tree = new OneWayFunctionTree();

        tree.root = new Node();
        tree.root.leftChild = new Node(tree.root);
        tree.root.rightChild = new Node(tree.root);
        tree.root.leftChild.leftChild = new Node(tree.root.leftChild);

        Assert.assertEquals(tree.root.rightChild, tree.findShallowestLeafNode(tree.root, 0).getKey());
    }

    @Test
    public void findShallowestNodeFour() {
        OneWayFunctionTree tree = new OneWayFunctionTree();

        tree.root = new Node();
        tree.root.leftChild = new Node(tree.root);
        tree.root.rightChild = new Node(tree.root);
        tree.root.leftChild.leftChild = new Node(tree.root.leftChild);
        tree.root.rightChild.leftChild = new Node(tree.root.rightChild);

        Assert.assertEquals(tree.root.leftChild.leftChild, tree.findShallowestLeafNode(tree.root, 0).getKey());
    }

}