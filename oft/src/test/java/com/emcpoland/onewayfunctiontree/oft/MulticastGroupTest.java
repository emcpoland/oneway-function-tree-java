package com.emcpoland.onewayfunctiontree.oft;

import com.emcpoland.onewayfunctiontree.MulticastGroup;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Arrays;

/**
 * Created by mcp on 21/11/15.
 */
public class MulticastGroupTest {

    @Test
    public void multiGroupSimpleAdd() {
        MulticastGroup group = new MulticastGroup();

        final Node[] endPoints = addEndpoints(group, 3);

        MulticastGroup.MGRekeyed mgRekeyed = Mockito.mock(MulticastGroup.MGRekeyed.class);
        group.setMGRekeyed(mgRekeyed);

        //System.out.println("About to add it now -----------------------");
        MulticastGroup.NodeKey oldKeyNode = new MulticastGroup.NodeKey(group.tree.root.rightChild.id.toString(), group.tree.root.rightChild.key);
        final Node newNode = group.add();

        /////////////////////////////////////////////////////////////////// Amount of executions ///

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey[].class));

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateSiblingEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

        Mockito.verify(mgRekeyed, Mockito.times(2)).updateMulticastKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

        ////////////////////////////////////////////////////////////////////////////////////////////

        MulticastGroup.NodeKey[] siblingSetKey = new MulticastGroup.NodeKey[]{
                new MulticastGroup.NodeKey(group.tree.root.rightChild.leftChild),
                new MulticastGroup.NodeKey(group.tree.root.leftChild)
        };

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateEndpointKey(
                Mockito.eq(new MulticastGroup.NodeKey(newNode)),
                Mockito.eq(siblingSetKey));

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateSiblingEndpointKey(
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.rightChild)),
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.rightChild.leftChild)));

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateMulticastKey(
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.leftChild)),
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.rightChild)));

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateMulticastKey(
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.rightChild.leftChild)),
                Mockito.eq(new MulticastGroup.NodeKey(group.tree.root.rightChild.rightChild)));

        ////////////////////////////////////////////////////////////////////////////////////////////



        //ArgumentCaptor<SubGroup> subGroupCaptor = ArgumentCaptor.forClass(SubGroup.class);
        //ArgumentCaptor<SubGroup[]> subGroupsCaptor = ArgumentCaptor.forClass(SubGroup[].class);
        //Mockito.verify(mgRekeyed, Mockito.times(1)).updateMulticastKey(subGroupCaptor.capture(), subGroupsCaptor.capture());
        //Assert.assertTrue(subGroupCaptor.getAllValues().equals(Arrays.asList(new S)));
        /*ArgumentCaptor<NodeKey> subGroupCaptor = ArgumentCaptor.forClass(SubGroup.class);
        Mockito.verify(mgRekeyed, Mockito.times(1)).updateMulticastKey(subGroupCaptor.capture(), Mockito.any(SubGroup[].class));
        Assert.assertTrue(subGroupCaptor.getValue().getID().equals(group.tree.root.leftChild.getId().toString()));*/
    }

    @Test
    public void emptyGroupNoAdd() {

        MulticastGroup.MGRekeyed mgRekeyed = Mockito.mock(MulticastGroup.MGRekeyed.class);
        MulticastGroup group = new MulticastGroup(mgRekeyed);

        /////////////////////////////////////////////////////////////////// Amount of executions ///

        Mockito.verify(mgRekeyed, Mockito.never()).updateEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey[].class));

        Mockito.verify(mgRekeyed, Mockito.never()).updateSiblingEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

        Mockito.verify(mgRekeyed, Mockito.never()).updateMulticastKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

    }

    @Test
    public void singleGroupSimpleAdd() {

        MulticastGroup.MGRekeyed mgRekeyed = Mockito.mock(MulticastGroup.MGRekeyed.class);
        MulticastGroup group = new MulticastGroup(mgRekeyed);

        final Node newNode = group.add();

        /////////////////////////////////////////////////////////////////// Amount of executions ///

        Mockito.verify(mgRekeyed, Mockito.times(1)).updateEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey[].class));

        Mockito.verify(mgRekeyed, Mockito.never()).updateSiblingEndpointKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

        Mockito.verify(mgRekeyed, Mockito.never()).updateMulticastKey(
                Mockito.any(MulticastGroup.NodeKey.class),
                Mockito.any(MulticastGroup.NodeKey.class));

    }

    private static Node[] addEndpoints(MulticastGroup group, int endPointCount) {
        Node[] endPoints = new Node[endPointCount];
        for(int i=0; i<endPointCount; i++) {
            endPoints[i] = group.add();
        }
        return endPoints;
    }

}
