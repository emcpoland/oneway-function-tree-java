package com.emcpoland.onewayfunctiontree.oft;

/**
 * Created by mcp on 29/11/15.
 */
class SimpleOFTKeyInterface implements OneWayFunctionTree.OFTKeyInterface {
    private Node[] leafs;

    public Node[] getLeafs() {
        return leafs;
    }

    @Override
    public void nodeSwapped(Node old, Node newNode) {
        if(leafs!=null) {
            for (int i = 0; i < leafs.length; i++) {
                if (leafs[i].equals(old)) {
                    System.out.println("Replace "+old +" with "+newNode);
                    leafs[i] = newNode;
                }
            }
        }
    }
    @Override
    public void nodeCreated(Node node) {
        if(leafs==null) {
            leafs = new Node[]{node};
        }
        else {
            Node[] newleafs = new Node[leafs.length+1];
            System.arraycopy(leafs, 0, newleafs, 0, leafs.length);
            newleafs[newleafs.length-1] = node;
            leafs = newleafs;
        }
    }
    @Override
    public void notifySiblingThatNodeWasRekeyed(Node nodeToAlert) {}

}
