package com.emcpoland.onewayfunctiontree.oft;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * To work on unit tests
 */
public class RemoveNodeTest {
    private OneWayFunctionTree tree;
    private SimpleOFTKeyInterface simpleOFTKeyInterface;

    @Before
    public void setUp() {
        simpleOFTKeyInterface = new SimpleOFTKeyInterface();
        tree = new OneWayFunctionTree(simpleOFTKeyInterface);
    }

    @After
    public void tearDown() {
        tree = null;
        simpleOFTKeyInterface = null;
    }

    @Test
    public void removeOneEndpoint() {
        OFTUtils.addLeafs(tree, 1);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertTrue(tree.contains(leafs[0]));
        tree.remove(leafs[0]);
        Assert.assertFalse(tree.contains(leafs[0]));
    }

    @Test
    public void removeTwoEndpoint() {
        OFTUtils.addLeafs(tree, 2);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertTrue(tree.contains(leafs[0]));
        Assert.assertTrue(tree.contains(leafs[1]));
        tree.remove(leafs[0]);
        Assert.assertFalse(tree.contains(leafs[0]));
        Assert.assertTrue(tree.contains(leafs[1]));
    }

    @Test
    public void removeThreeEndpoint() {
        OFTUtils.addLeafs(tree, 3);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertTrue(tree.contains(leafs[0]));
        Assert.assertTrue(tree.contains(leafs[1]));
        Assert.assertTrue(tree.contains(leafs[2]));
        tree.remove(leafs[0]);
        Assert.assertFalse(tree.contains(leafs[0]));
        Assert.assertTrue(tree.contains(leafs[1]));
        Assert.assertTrue(tree.contains(leafs[2]));
    }
    
}