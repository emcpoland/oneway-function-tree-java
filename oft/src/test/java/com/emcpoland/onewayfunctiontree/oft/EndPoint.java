package com.emcpoland.onewayfunctiontree.oft;

import com.emcpoland.onewayfunctiontree.MulticastGroup;
import com.sun.javafx.tools.packager.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mcp on 29/11/15.
 */
class EndPoint {
    MulticastGroup.NodeKey nodeKey;
    MulticastGroup.NodeKey siblingNodeKey;
    Map<String, MulticastGroup.NodeKey> otherNodeKeys;
    byte[] groupKey;

    EndPoint(MulticastGroup.NodeKey nodeKey, MulticastGroup.NodeKey[] otherNodeKeys) {
        //System.out.println("Created endpoint with key " + nodeKey + " and keys  " + Arrays.toString(otherNodeKeys));
        this.nodeKey = nodeKey;
        this.otherNodeKeys = new LinkedHashMap<>(); //LinkedHashMap retains order of insertion

        if(otherNodeKeys.length>0) {
            this.siblingNodeKey = otherNodeKeys[0];
            for (int i = 1; i < otherNodeKeys.length; i++) {
                this.otherNodeKeys.put(otherNodeKeys[i].getId(), otherNodeKeys[i]);
            }
        }

        rekey();
    }

    public void setNodeKey(MulticastGroup.NodeKey nodeKey) {
        //System.out.println("Update sibling endpoint key " + this + " --> " + nodeKey);
        this.nodeKey = nodeKey;
        rekey();
    }

    public void input(MulticastGroup.NodeKey publishNode, MulticastGroup.NodeKey nodeChanged) {
        // Assuming publish key is this key, so should know it
        if(nodeKey.equals(publishNode) && Arrays.equals(nodeKey.getKey(), publishNode.getKey())) {
            if(siblingNodeKey!=null) {
                //Need to add the old sibling to uncle list now. But need to add at start which causes this issue in the first place
                //this.otherNodeKeys.put(siblingNodeKey.getId(), siblingNodeKey);
                Map<String,MulticastGroup.NodeKey> cloneotherkeys = new LinkedHashMap<>();
                cloneotherkeys.put(siblingNodeKey.getId(), siblingNodeKey);
                this.otherNodeKeys.remove(siblingNodeKey.getId());
                cloneotherkeys.putAll(otherNodeKeys);
                otherNodeKeys = cloneotherkeys;
            }
            this.otherNodeKeys.remove(nodeChanged.getId());
            this.siblingNodeKey = nodeChanged;
            rekey();
        }
        // && otherNodeKeys.get(nodeChanged.getId()).getKey().equals(nodeChanged.getKey())
        // gonna need to find a way to figure out the key required for this
        // All transient keys
        // get this node key
        // get the on before it,
        // find the keys up until this point
        // need to rekey a subset list of these keys
        // replace null with this value
        else if(otherNodeKeys.containsValue(nodeChanged) && Arrays.equals(otherNodeKeys.get(nodeChanged.getId()).siblingPublishKey, publishNode.getKey())) {
            this.otherNodeKeys.put(nodeChanged.getId(), nodeChanged);
            rekey();
        }
        else {
            //System.out.println(this+" not the intended recipent");
        }
    }

    private void rekey() {
        this.groupKey = this.nodeKey.getKey();
        if(siblingNodeKey!=null) this.groupKey = mix(hash(this.groupKey), siblingNodeKey.getKey());
        //System.out.println("Start rekey");
        for(MulticastGroup.NodeKey nk:otherNodeKeys.values()) {
            //System.out.println("NodeKey: "+nk);
            nk.siblingPublishKey = Arrays.copyOf(this.groupKey, this.groupKey.length);
            this.groupKey = mix(hash(this.groupKey), nk.getKey());
        }
        //System.out.println("End rekey\n");
    }

    @Override
    public String toString() {
        return nodeKey.toString();
    }

    private static byte[] mix(byte[] left, byte[] right) {
        if(left.length!=right.length) throw new RuntimeException("keys not equal length");
        byte[] mixed = new byte[left.length];
        for(int i = 0; i<left.length; i++) {
            mixed[i] = (byte)(left[i]^right[i]);
        }
        return mixed;
    }

    private static byte[] hash(byte[] input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(input);
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
