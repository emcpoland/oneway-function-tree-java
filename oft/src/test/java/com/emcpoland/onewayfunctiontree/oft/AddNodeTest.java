package com.emcpoland.onewayfunctiontree.oft;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * To work on unit tests
 */
public class AddNodeTest {
    private OneWayFunctionTree tree;
    private SimpleOFTKeyInterface simpleOFTKeyInterface;

    @Before
    public void setUp() {
        simpleOFTKeyInterface = new SimpleOFTKeyInterface();
        tree = new OneWayFunctionTree(simpleOFTKeyInterface);
    }

    @After
    public void tearDown() {
        tree = null;
        simpleOFTKeyInterface = null;
    }

    @Test
    public void ensureTreeEmpty() {
        Assert.assertNull(tree.root);
    }

    ////////////////////////////////////////////////////////////////// Level 1

    @Test
    public void addOneEndpoint() {
        OFTUtils.addLeafs(tree, 1);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root);
    }


    ////////////////////////////////////////////////////////////////// Level 2

    @Test
    public void addTwoEndpoints() {
        OFTUtils.addLeafs(tree, 2);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild);
    }

    ////////////////////////////////////////////////////////////////// Level 3

    @Test
    public void addThreeEndpoints() {
        OFTUtils.addLeafs(tree, 3);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild);
    }

    @Test
    public void addFourEndpoints() {
        OFTUtils.addLeafs(tree, 4);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild);
    }

    ////////////////////////////////////////////////////////////////// Level 4

    @Test
    public void addFiveEndpoints() {
        OFTUtils.addLeafs(tree, 5);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild);
    }

    @Test
    public void addSixEndpoints() {
        OFTUtils.addLeafs(tree, 6);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild);
    }

    @Test
    public void addSevenEndpoints() {
        OFTUtils.addLeafs(tree, 7);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild);
    }

    @Test
    public void addEightEndpoints() {
        OFTUtils.addLeafs(tree, 8);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    ////////////////////////////////////////////////////////////////// Level 5

    @Test
    public void addNineEndpoints() {
        OFTUtils.addLeafs(tree, 9);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addTenEndpoints() {
        OFTUtils.addLeafs(tree, 10);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addElevenEndpoints() {
        OFTUtils.addLeafs(tree, 11);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addTwelveEndpoints() {
        OFTUtils.addLeafs(tree, 12);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[11], tree.root.leftChild.rightChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addThirteenEndpoints() {
        OFTUtils.addLeafs(tree, 13);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[11], tree.root.leftChild.rightChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[12], tree.root.rightChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addFourteenEndpoints() {
        OFTUtils.addLeafs(tree, 14);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[11], tree.root.leftChild.rightChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[12], tree.root.rightChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[13], tree.root.rightChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addFithteenEndpoints() {
        OFTUtils.addLeafs(tree, 15);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[11], tree.root.leftChild.rightChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[12], tree.root.rightChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[13], tree.root.rightChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[14], tree.root.rightChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild);
    }

    @Test
    public void addSixteenEndpoints() {
        OFTUtils.addLeafs(tree, 16);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Assert.assertEquals(leafs[0], tree.root.leftChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[8], tree.root.leftChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[4], tree.root.leftChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[9], tree.root.leftChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[2], tree.root.leftChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[10], tree.root.leftChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[5], tree.root.leftChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[11], tree.root.leftChild.rightChild.rightChild.rightChild);
        Assert.assertEquals(leafs[1], tree.root.rightChild.leftChild.leftChild.leftChild);
        Assert.assertEquals(leafs[12], tree.root.rightChild.leftChild.leftChild.rightChild);
        Assert.assertEquals(leafs[6], tree.root.rightChild.leftChild.rightChild.leftChild);
        Assert.assertEquals(leafs[13], tree.root.rightChild.leftChild.rightChild.rightChild);
        Assert.assertEquals(leafs[3], tree.root.rightChild.rightChild.leftChild.leftChild);
        Assert.assertEquals(leafs[14], tree.root.rightChild.rightChild.leftChild.rightChild);
        Assert.assertEquals(leafs[7], tree.root.rightChild.rightChild.rightChild.leftChild);
        Assert.assertEquals(leafs[15], tree.root.rightChild.rightChild.rightChild.rightChild);
    }

    @Test
    public void consistentTree() {
        OneWayFunctionTree tree = new OneWayFunctionTree();
        int noLevels = 8;
        int noNodes = (int)Math.pow(2, noLevels)-1;
        int noLeafs = (noNodes+1)/2;

        Node[] leafs = OFTUtils.addLeafs(tree, noLeafs);

        Assert.assertEquals(noNodes, tree.count());
        Assert.assertTrue(OFTUtils.isValidTree(tree.root));
    }


}