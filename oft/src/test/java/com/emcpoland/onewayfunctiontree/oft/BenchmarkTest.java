package com.emcpoland.onewayfunctiontree.oft;

import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkHistoryChart;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.junitbenchmarks.annotation.LabelType;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by mcp on 30/01/16.
 */
@RunWith(Parameterized.class)
//@BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
//@AxisRange(min = 0, max = 1)
//@BenchmarkMethodChart(filePrefix = "hash-benchmark")
@BenchmarkHistoryChart(filePrefix = "hash-benchmark", labelWith = LabelType.CUSTOM_KEY, maxRuns = 20)
public class BenchmarkTest extends AbstractBenchmark {
    private int numberOfLeafNodes;
    /*private final OneWayFunctionTree tree;
    private final Node[] leafNodes;*/

    static {
        System.setProperty("jub.customkey", "BLAKE2");
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {10}, {20}, {30}, {40}, {50}, {60}, {70}, {80}, {90}, {100},
                {200}, {300}, {400}, {500}, {600}, {700}, {800}, {900}, {1000},
                {2000}, {3000}, {4000}, {5000}, {6000}, {7000}, {8000}, {9000}, {10000}
        });
    }

    public BenchmarkTest(int numberOfLeafNodes) {
        this.numberOfLeafNodes = numberOfLeafNodes;
        /*this.tree = new OneWayFunctionTree();
        this.leafNodes = OFTUtils.addLeafs(tree, numberOfLeafNodes);*/
    }

    @Test
    public void testAdd() throws Exception {
        OFTUtils.addLeafs(new OneWayFunctionTree(), numberOfLeafNodes);
    }
}
