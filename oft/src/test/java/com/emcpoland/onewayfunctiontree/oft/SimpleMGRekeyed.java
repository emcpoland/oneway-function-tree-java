package com.emcpoland.onewayfunctiontree.oft;

import com.emcpoland.onewayfunctiontree.MulticastGroup;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mcp on 29/11/15.
 */
class SimpleMGRekeyed implements MulticastGroup.MGRekeyed {
    private Map<String,EndPoint> endPoints = new HashMap<String,EndPoint>();

    @Override
    public void updateEndpointKey(MulticastGroup.NodeKey publishNode, MulticastGroup.NodeKey[] nodes) {
        endPoints.put(publishNode.getId(), new EndPoint(publishNode, nodes));
    }

    @Override
    public void updateSiblingEndpointKey(MulticastGroup.NodeKey oldNode, MulticastGroup.NodeKey newNode) {
        EndPoint ep = endPoints.remove(oldNode.getId());
        ep.setNodeKey(newNode);
        endPoints.put(newNode.getId(), ep);
    }

    @Override
    public void updateMulticastKey(MulticastGroup.NodeKey publishNode, MulticastGroup.NodeKey nodeChanged) {
        for(EndPoint endPoint:endPoints.values()) {
            endPoint.input(publishNode, nodeChanged);
        }
    }

    public Map<String,EndPoint> getEndPoints() {
        return endPoints;
    }
}
