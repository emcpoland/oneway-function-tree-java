package com.emcpoland.onewayfunctiontree.oft;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by mcp on 19/11/15.
 */
public class UncleTest {

    @Test
    public void checkLeftMostLeafUncles() {
        SimpleOFTKeyInterface simpleOFTKeyInterface = new SimpleOFTKeyInterface();
        OneWayFunctionTree tree = new OneWayFunctionTree(simpleOFTKeyInterface);

        OFTUtils.addLeafs(tree, 16);
        Node[] leafs = simpleOFTKeyInterface.getLeafs();

        Node[] uncles = new Node[]{
                tree.root.leftChild.leftChild.rightChild,
                tree.root.leftChild.rightChild,
                tree.root.rightChild
        };

        Assert.assertEquals(Arrays.asList(uncles), leafs[0].getUncleSet());
        Assert.assertEquals(Arrays.asList(uncles), leafs[8].getUncleSet()); // Left most and second left most uncles are equal
    }

    @Test
    public void checkMiddleLeafUncles() {
        OneWayFunctionTree tree = new OneWayFunctionTree();
        Node[] leafs = OFTUtils.addLeafs(tree, 16);

        Node[] uncles = new Node[]{
                tree.root.leftChild.rightChild.leftChild,
                tree.root.leftChild.leftChild,
                tree.root.rightChild
        };

        Assert.assertEquals(Arrays.asList(uncles), leafs[11].getUncleSet());
    }

}
