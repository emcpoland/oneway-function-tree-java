package com.emcpoland.onewayfunctiontree;

import com.emcpoland.onewayfunctiontree.oft.Node;
import com.emcpoland.onewayfunctiontree.oft.OneWayFunctionTree;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by mcp on 17/11/15.
 */
public class MulticastGroup implements OneWayFunctionTree.OFTKeyInterface {
    public OneWayFunctionTree tree = new OneWayFunctionTree(this);
    private MGRekeyed mgRekeyed;

    public MulticastGroup() {}

    public MulticastGroup(MGRekeyed mgRekeyed) {
        this.mgRekeyed = mgRekeyed;
    }

    public void setMGRekeyed(MGRekeyed mgRekeyed) {
        this.mgRekeyed = mgRekeyed;
    }

    public static class NodeKey {
        private String id;
        private byte[] key;
        public byte[] siblingPublishKey;

        public  NodeKey(String id, byte[] key) {
            this.id = id;
            this.key = key;
        }

        public NodeKey(Node node) {
            this.id = node.getId();
            this.key = node.getKey();
        }

        public String getId() {
            return id;
        }

        public byte[] getKey() {
            return key;
        }

        @Override
        public boolean equals(Object o) {
            return (o instanceof NodeKey && ((NodeKey) o).id.equals(this.id));
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        @Override
        public String toString() {
            return id + " {" + Base64.encode(key) + "}";
        }
    }





    public interface MGRekeyed {
        void updateEndpointKey(NodeKey publishNode, NodeKey[] nodes); //unicast only one per registration
        void updateSiblingEndpointKey(NodeKey oldNode, NodeKey newNode); //unicast only one per registration
        void updateMulticastKey(NodeKey publishNode, NodeKey nodeChanged);// Nodes will both be siblings, but lets make it clear
    }

    public Node add() {
        return tree.add();
    }

    public void remove(Node node) {
        tree.remove(node);
    }

    @Override
    public void nodeCreated(Node node) {
        List<NodeKey> siblingNodeKeys = new ArrayList<>();

        if(!node.isRoot()) {
            siblingNodeKeys.add(new NodeKey(node.getSibling().getId(), node.getSibling().getBlindedKey()));
            for (Node uNode : node.getUncleSet()) {
                siblingNodeKeys.add(new NodeKey(uNode.getId(), uNode.getBlindedKey()));
            }
        }

        NodeKey[] sNodeKeys = siblingNodeKeys.toArray(new NodeKey[siblingNodeKeys.size()]);

        if(mgRekeyed!=null) mgRekeyed.updateEndpointKey(new NodeKey(node.getId(), node.getKey()), sNodeKeys);
    }


    @Override
    public void nodeSwapped(Node oldNode, Node newNode) {
        if(mgRekeyed!=null) mgRekeyed.updateSiblingEndpointKey(new NodeKey(oldNode.getId(), oldNode.getKey()), new NodeKey(newNode.getId(), newNode.getKey()));
    }

    @Override
    public void notifySiblingThatNodeWasRekeyed(Node nodeToAlert) {
        if(mgRekeyed!=null) mgRekeyed.updateMulticastKey(
                new NodeKey(nodeToAlert.getId(), nodeToAlert.getKey()),
                new NodeKey(nodeToAlert.getSibling().getId(), nodeToAlert.getSibling().getBlindedKey()));
    }

}
