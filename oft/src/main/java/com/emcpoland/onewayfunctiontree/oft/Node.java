package com.emcpoland.onewayfunctiontree.oft;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import org.bouncycastle.crypto.digests.Blake2bDigest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.jcajce.provider.digest.Blake2b;
import org.bouncycastle.jcajce.provider.digest.SHA256;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.crypto.KeyGenerator;

import sun.security.provider.SHA2;

public class Node {
    UUID id = UUID.randomUUID();
    Node parent;
    Node leftChild;
    Node rightChild;

    byte[] key;
    byte[] blindedKey;

    Node () {
        rekey();
    }

    Node(Node parent) {
        this();
        this.parent = parent;
    }

    public byte[] getKey() {
        return key;
    }

    public byte[] getBlindedKey() {
        return blindedKey;
    }

    public List<byte[]> getKeys() {
        List<byte[]> keys = new ArrayList<>();
        keys.add(this.key);

        keys.add(getSibling().blindedKey);

        for(Node node:getUncleSet()) {
            keys.add(node.blindedKey);
        }

        return keys;
    }

    public boolean isRoot() {
        return parent == null;
    }

    boolean contains(Node node) {
        return contains(this, node);
    }

    public Node getSibling() {
        return (parent.leftChild==this) ? parent.rightChild : parent.leftChild;
    }

    public List<Node> getUncleSet() {
        return getUncleSet(parent, new ArrayList<Node>());
    }

    public String getId() {
        return id.toString();
    }

    int count() {
        return count(this, 0);
    }

    void rekey() {
        setKey(generator.generateKey().getEncoded());
    }

    void setKey(byte[] key) {
        this.key = key;
        this.blindedKey = hash(this.key);
    }

    @Override
    public String toString() {
        return id+"_{"+new String(Base64.encode(key))+"}_{"+new String(Base64.encode(blindedKey)+"}");
    }

    private static boolean contains(Node root, Node node) {
        if (root==node) return true;
        else if(root==null) return false;
        else return contains(root.leftChild, node) || contains(root.rightChild, node);
    }

    private static List<Node> getUncleSet(Node node, List<Node> previousUncles) {
        if(!node.isRoot()) {
            previousUncles.add(node.getSibling());
            getUncleSet(node.parent, previousUncles);
        }
        return previousUncles;
    }

    private static int count(Node node, int count) {
        return  (node == null) ? 0 : 1+count(node.leftChild, count + 1)+count(node.rightChild, count + 1);
    }

    ///////////// Crypto stuff

    private static final KeyGenerator generator = buildGenerator();
    private static KeyGenerator buildGenerator() {
        try {
            KeyGenerator generator = KeyGenerator.getInstance("AES");
            generator.init(256, new SecureRandom());
            generator.generateKey();
            return generator;
        } catch (Exception e) {
            return null;
        }
    }

    //private static final SHA256.Digest messageDigest = new SHA256.Digest();
    //private static final SHA3.DigestSHA3 messageDigest = new SHA3.Digest256();
    private static final Blake2b.Blake2b256 messageDigest = new Blake2b.Blake2b256();
    private static byte[] hash(byte[] input) {
        messageDigest.update(input, 0, input.length);
        return messageDigest.digest();
    }
}