package com.emcpoland.onewayfunctiontree.oft;

import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * Created by mcp on 18/11/15.
 */
class OFTUtils {

    public static Node[] addLeafs(OneWayFunctionTree tree, int leafCount) {
        Node[] leafs = new Node[leafCount];
        for(int i=0; i<leafCount; i++) leafs[i] = tree.add();
        return leafs;
    }

    static boolean isValidTree(Node root) {
        return isValidTree(null, root);
    }

    private static boolean isValidTree(Node parent, Node node) {
        if(node == null) return true;
        else return node.parent == parent  && isValidTree(node, node.leftChild) && isValidTree(node, node.rightChild);
    }

    public static void logNode(Node node) {
        System.out.println("[" + node.parent + "[" + node + "[" + node.leftChild + " " + node.rightChild + "]]]");
    }

    public static void logTree(Node root) {
        System.out.println(cascadingNodeString(root));
    }

    private static String cascadingNodeString(Node node) {
        if(node==null) return null;
        return "[" + node + "[" + cascadingNodeString(node.leftChild) + " " + cascadingNodeString(node.rightChild) + "]]";
    }

    public static void logKey(byte[] key) {
        System.out.println(getKeyString(key));
    }

    public static String getKeyString(byte[] key) {
        return new String(Base64.encode(key));
    }

}
