package com.emcpoland.onewayfunctiontree.oft;

import java.util.HashSet;
import java.util.Set;

import javafx.util.Pair;

/**
 * Created by mcp on 17/11/15.
 */
public class OneWayFunctionTree {
    Node root;
    Set<Node> leafNodes = new HashSet();

    private OFTKeyInterface keyInterface;
    public interface OFTKeyInterface {
        void nodeCreated(Node node);
        void nodeSwapped(Node old, Node newNode);
        void notifySiblingThatNodeWasRekeyed(Node nodeToAlert);
    }

    public OneWayFunctionTree() {}

    public OneWayFunctionTree(OFTKeyInterface keyInterface) {
        this.keyInterface = keyInterface;
    }

    public Node add() {
        // Add initial node
        if(root == null) {
            root = new Node();
            if(keyInterface!=null) keyInterface.nodeCreated(root);
            return root;
        }

        Node currentLeafNodeNewParent = findShallowestLeafNode(root, 0).getKey();

        Node newLeafNodeA = new Node(currentLeafNodeNewParent);
        Node newLeafNodeB = new Node(currentLeafNodeNewParent);

        if(newLeafNodeA.blindedKey == null) throw new RuntimeException("newLeafNodeA is null");
        if(newLeafNodeB.blindedKey  == null) throw new RuntimeException("newLeafNodeB is null");

        currentLeafNodeNewParent.leftChild = newLeafNodeA;
        currentLeafNodeNewParent.rightChild = newLeafNodeB;

        if(keyInterface!=null) {
            keyInterface.nodeCreated(newLeafNodeB);
            keyInterface.nodeSwapped(currentLeafNodeNewParent, newLeafNodeA);
            keyInterface.notifySiblingThatNodeWasRekeyed(newLeafNodeA);
        }

        rekeyAncestors(currentLeafNodeNewParent);

        return newLeafNodeB;
    }

    void rekeyAncestors(Node ancestorNode) {
        //need to pass the previous node to get the sibling to say multicast to using treerekeyed
        //ancestorNode.setKey(ancestorNode.rightChild.key);
        ancestorNode.setKey(mix(ancestorNode.leftChild.blindedKey, ancestorNode.rightChild.blindedKey));
        if(!ancestorNode.isRoot()) {
            if(keyInterface!=null) keyInterface.notifySiblingThatNodeWasRekeyed(ancestorNode.getSibling());
            rekeyAncestors(ancestorNode.parent);
        }
    }

    public void remove(Node node) {
        if (node == root) {
            root = null;
        }
        else {
            if(node.parent == root) {
                boolean isLeftChild = (node == node.parent.leftChild);
                if(isLeftChild) {
                    root = node.parent.rightChild;
                }
                else {
                    root = node.parent.leftChild;
                }
            }
            else {
                boolean isParentLeftChild = (node.parent == node.parent.parent.leftChild);
                if (isParentLeftChild) {
                    node.parent.parent.leftChild = node.parent.rightChild;
                    node.parent.parent.leftChild.parent = node.parent.parent.rightChild;
                } else {
                    node.parent.parent.rightChild = node.parent.leftChild;
                    node.parent.parent.rightChild.parent = node.parent.parent.leftChild;
                }

                node.parent.leftChild = null;
                node.parent.rightChild = null;
                node.parent.parent = null;
                node.parent = null;
            }
        }
    }

    Pair<Node, Integer> findShallowestLeafNode(Node node, int count) {
        if(node.leftChild == null && node.rightChild == null) return new Pair<>(node, count); //isLeaf

        count++;
        // Need to see which is shallowest
        Pair shallowestNodePairLeft = findShallowestLeafNode(node.leftChild, count);
        if(node.rightChild == null) return shallowestNodePairLeft;

        Pair shallowestNodePairRight  = findShallowestLeafNode(node.rightChild, count);
        return ((int)shallowestNodePairLeft.getValue() <= (int)shallowestNodePairRight.getValue()) ? shallowestNodePairLeft : shallowestNodePairRight;
    }

    public boolean contains(Node node) {
        return root!=null && root.contains(node);
    }

    public int count() {
        return (root==null) ? 0 : root.count();
    }

    private static byte[] mix(byte[] left, byte[] right) {
        if(left == null) throw new RuntimeException("Left is null");
        if(right == null) throw new RuntimeException("Right is null");
        if(left.length!=right.length) throw new RuntimeException("keys not equal length ");
        byte[] mixed = new byte[left.length];
        for(int i = 0; i<left.length; i++) {
            mixed[i] = (byte)(left[i]^right[i]);
        }
        /*System.out.println("Implementation");
        OFTUtils.logKey(left);
        OFTUtils.logKey(right);
        OFTUtils.logKey(mixed);*/
        return mixed;
    }

}
