package com.emcpoland.onewayfunctiontree;

import com.emcpoland.onewayfunctiontree.oft.Node;

public class Main {

    public static void main(String [] args) {

        MulticastGroup group = new MulticastGroup(new MulticastGroup.MGRekeyed() {
            @Override
            public void updateEndpointKey(MulticastGroup.NodeKey publishNode, MulticastGroup.NodeKey[] nodeKeys) {
                System.out.println("Unicast to the new node " + publishNode);
                for(MulticastGroup.NodeKey nk:nodeKeys) {
                    System.out.println("----------------> " + nk);
                }
                System.out.println();
            }

            @Override
            public void updateSiblingEndpointKey(MulticastGroup.NodeKey oldNode, MulticastGroup.NodeKey newNode) {
                System.out.println("Unicast to the existing sibling node its new keys  and id " + newNode + ", encrypting it with its old key and referring to it using its old id " + oldNode);
                System.out.println();
            }

            @Override
            public void updateMulticastKey(MulticastGroup.NodeKey publishNode, MulticastGroup.NodeKey nodeChanged) {
                System.out.println("Multicast to "+publishNode+" the new updated key for "+nodeChanged);
                System.out.println();
            }
        });

        for(int i=0; i<10; i++) group.add();
    }

}